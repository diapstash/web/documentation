FROM node:lts as builder


WORKDIR /app
COPY . /app/

RUN yarn install
ENV NODE_ENV=production
RUN yarn build


FROM nginx:alpine

RUN apk add --update curl && \
    rm -rf /var/cache/apk/*

COPY nginx.conf /etc/nginx/conf.d/configfile.template
COPY --from=builder /app/build /usr/share/nginx/html
ENV PORT 8080
ENV HOST 0.0.0.0
EXPOSE 8080

HEALTHCHECK --interval=60s --timeout=3s CMD curl -f http://localhost:8080/ || false
CMD sh -c "envsubst '\$PORT' < /etc/nginx/conf.d/configfile.template > /etc/nginx/conf.d/default.conf && nginx -g 'daemon off;'"